package com.archeinteractive.nodrop;

import java.io.File;
import java.util.List;

import com.archeinteractive.nodrop.config.Settings;
import com.archeinteractive.nodrop.util.JsonConfig;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class NoDrop extends JavaPlugin {
    private static String name;
    private static String version;
    private static List<String> authors;
    protected NoDrop plugin;
    protected Settings settings;

    // Configuration Variables
    private boolean keepXP;
    private boolean keepItems;

    public void onEnable() {
        plugin = this;
        name = this.getDescription().getName();
        version = this.getDescription().getVersion();
        authors = this.getDescription().getAuthors();
        PluginManager pm = this.getServer().getPluginManager();
        NoDropEvents listener = new NoDropEvents(this);
        NoDropLogger.initialize(this.getLogger());

        if (!getDataFolder().exists()) {
            NoDropLogger.info("Config folder not found! Creating...");
            getDataFolder().mkdir();
        }

        settings = JsonConfig.load(new File(getDataFolder(), "settings.json"), Settings.class);

        // Load Events!
        pm.registerEvents(listener, this);
    }

    public static String getPluginName() {
        return name;
    }

    public static String getPluginVersion() {
        return version;
    }

    public static List<String> getPluginAuthors() {
        return authors;
    }

    public boolean getXpSetting() {
        return keepXP;
    }

    public boolean getItemSetting() {
        return keepItems;
    }
}
