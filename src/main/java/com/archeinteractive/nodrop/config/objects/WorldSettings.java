package com.archeinteractive.nodrop.config.objects;

import java.util.ArrayList;
import java.util.List;

public class WorldSettings {

    public List<Integer> itemIds = new ArrayList<Integer>(){{
        add(56);
        add(57);
        add(264);
        add(276);
        add(277);
        add(278);
        add(279);
        add(293);
        add(310);
        add(311);
        add(312);
        add(313);
    }};

}
