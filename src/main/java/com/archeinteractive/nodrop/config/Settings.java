package com.archeinteractive.nodrop.config;

import com.archeinteractive.nodrop.config.objects.WorldSettings;
import com.archeinteractive.nodrop.util.CaseInsensitiveHashMap;
import com.archeinteractive.nodrop.util.JsonConfig;

import java.util.Map;

public class Settings extends JsonConfig {

    public Map<String, WorldSettings> worlds = new CaseInsensitiveHashMap<WorldSettings>(){{
        put("world", new WorldSettings());
    }};

}
