package com.archeinteractive.nodrop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.archeinteractive.nodrop.config.objects.WorldSettings;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

public class NoDropEvents implements Listener {
    private final NoDrop PLUGIN;

    public NoDropEvents(NoDrop instance) {
        PLUGIN = instance;
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity().getPlayer();

        WorldSettings settings = PLUGIN.settings.worlds.get(player.getWorld().getName());
        if (settings != null) {
            List<ItemStack> matching = new ArrayList<ItemStack>();
            for (ItemStack itemStack : event.getDrops()) {
                if (settings.itemIds.contains(itemStack.getTypeId())) {
                    matching.add(itemStack);
                }
            }
            event.getDrops().removeAll(matching);
        }
    }
}
